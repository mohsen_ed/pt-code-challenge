import jwt
import time
from fastapi import Response, Request, Header, HTTPException
import hashlib

def validate_jwt_token(auth_token = Header("Authorization")):
    if auth_token is not None:
        auth_token = auth_token.split(" ")
        if len(auth_token) == 2:
            user_info = jwt.decode(auth_token, "my_secret_key", algorithm="HS256")

            if user_info and user_info["exp"] > time.time():
                return user_info

    raise HTTPException(403, "Authentication failed")


def check_password(password, hashed_password):
    return hashlib.md5(password.encode('utf-8')).hexdigest() == hashed_password
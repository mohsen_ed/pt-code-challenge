from sqlalchemy import create_engine, select, MetaData, Table, Column, String, Text, Integer
from sqlalchemy.orm import Session
import hashlib

from models import Post, User

engine = create_engine('sqlite:///db.sqlite', echo=True)
session = Session(engine)

def get_session() -> Session:
    return session

def find_post_by_id(id):
    session = get_session()
    stmt = select(Post).where(Post.id == id)
    post = session.scalars(stmt).one()

    return post

def find_user_by_email(email):
    session = get_session()
    stmt = select(User).where(User.email == email)
    user = session.scalars(stmt).one()

    return user

def create_post(post: Post):
    session = get_session()
    session.add(post)
    session.commit()
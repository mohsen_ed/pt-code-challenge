import time
import jwt
from typing import Annotated
from sqlalchemy import select
from fastapi import FastAPI, Depends
from fastapi.security import HTTPBearer

from db import get_session, find_post_by_id, create_post, find_user_by_email
from models import PostRequest, Post, AuthRequest, User
from deps import validate_jwt_token, check_password

app = FastAPI()

@app.get("/ping")
def ping_action():
    return {"pong": True, "timestamp": time.time()}

@app.get("/posts")
def list_action():
    stmt = select(Post)
    result = get_session().scalars(stmt)
    return result.all()

@app.post("/posts")
def create_action(data: PostRequest):
    post = Post(title=data.title, description=data.description)

    try:
        create_post(post)
    except Exception as e:
        return {
            "ok": False,
            "message": str(e)
        }

    return {
        "ok": True,
        "message": "Post created successfully",
    }

@app.put("/posts/{id}")
def update_action(id, data: PostRequest, user = Depends(validate_jwt_token)):
    post = find_post_by_id(id)
    if not post:
        return {
            "ok": False,
            "message": "The requested post doesnt exists"
        }

    post.title = data.title
    post.description = data.description
    get_session().commit()

    return {
        "ok": True,
        "post": post,
    }

@app.delete("/posts/{id}")
def delete_action(id):
    post = find_post_by_id(id)
    if not post:
        return {"message": "The requested post doesnt exists"}

    get_session().delete(post)

    return {
        "ok": True,
        "message": "The specified post deleted successfully",
    }

@app.post("/auth")
def auth_action(request: AuthRequest):
    try:
        user = find_user_by_email(request.email)
        print(user)
        if check_password(request.password, user.password):
            exp_value = int(time.time()) + 3600
            token = jwt.encode({"sub": 1, "exp": exp_value}, "my_secret_key", algorithm="HS256")

            return {
                "ok": True, 
                "token": token
            }

        return {
            "ok": False, 
            "message": "Invalid Credentials"
        }
    except:
        return {
            "ok": False, 
            "message": "Invalid Credentials"
        }

auth_scheme = HTTPBearer()
@app.get("/auth")
async def echo_me(token = Depends(auth_scheme)):
    pass
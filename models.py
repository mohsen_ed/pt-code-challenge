from typing import Optional
from pydantic import BaseModel
from sqlalchemy import String, Text, UniqueConstraint
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

class DBModel(DeclarativeBase):
    pass

class Post(DBModel):
    __tablename__ = "posts"
    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String(length=64))
    description: Mapped[Optional[str]] = mapped_column(Text())

class User(DBModel):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True)
    email: Mapped[str] = mapped_column(String(length=255))
    password: Mapped[str] = mapped_column(String(length=255))
    __table_args__ = (UniqueConstraint("email", name="user_email-unique"),)


class PostRequest(BaseModel):
    title: str
    description: str

class AuthRequest(BaseModel):
    email: str
    password: str